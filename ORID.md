Objective:
1.	We did a presentation this morning, which was the first try of our team. We took responsibility for our own work and cooperated with each other at the same time.
2.	We learned “Refactor” in the afternoon. What impressed me most was the Code Smell part, which reminded me of the bad coding habits I have.

Reflective:
1.	I feel good because we worked hard to cooperate with each other in our teamwork and finally finished the task well.
2.	I feel happy because I can polish my codes in the future and make my code look more elegant.

Interpretive:
1.	I feel that no matter how difficult a task is, we need to put our team’s benefit to the first place so that we can cooperate with each other and finish our tasks better.
2.	Before learning, I may have never known that I have so many problems in my codes, so that the courses today are very important to me to improve my coding abilities to write vigorous codes.

Decisional:
1.	I decided to continue the spirit of cooperation in my future coding career in order to better cooperate with my colleagues to benefit the software development of the company.
2.	I decided to reduce the code smell in my code and generate more elegant code in order to make it convenient to read my codes.
