import java.util.Comparator;

public class Input {
    private final String value;
    private final int count;

    public Input(String value, int count){
        this.value =value;
        this.count =count;
    }


    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }

    public String getInputWordCountString() {
        return value + " " + count;
    }

    static Comparator<Input> InputWordCountDescComparator() {
        return (input1, input2) -> input2.getWordCount() - input1.getWordCount();
    }
}
