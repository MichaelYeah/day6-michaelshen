import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String REGEX_SPLIT = "\\s+";
    public static final String JOIN_DELIMITER = "\n";

    public String getResult(String inputStr) {
        try {
            return joinInputListToString(sortInputListDesc(splitStringToInputList(inputStr)));
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private List<Input> sortInputListDesc(List<Input> inputList) {
        return countWordFrequency(inputList).stream()
                .sorted(Input.InputWordCountDescComparator())
                .collect(Collectors.toList());
    }

    private List<Input> countWordFrequency(List<Input> inputList) {
        return getListMap(inputList).entrySet().stream()
                .map(stringListEntry -> new Input(stringListEntry.getKey(), stringListEntry.getValue().size()))
                .collect(Collectors.toList());
    }

    private List<Input> splitStringToInputList(String inputStr) {
        return Arrays.stream(splitStringToArray(inputStr))
                .map(str -> new Input(str, 1))
                .collect(Collectors.toList());
    }

    private String joinInputListToString(List<Input> inputList) {
        return inputList.stream()
                .map(Input::getInputWordCountString)
                .collect(Collectors.joining(JOIN_DELIMITER));
    }

    private String[] splitStringToArray(String inputStr) {
        return inputStr.split(REGEX_SPLIT);
    }

    private Map<String, List<Input>> getListMap(List<Input> inputList) {
        Map<String, List<Input>> map = new HashMap<>();
        inputList.forEach(input -> {
            if (!map.containsKey(input.getValue())) {
                List<Input> inputArrayList = new ArrayList<>();
                inputArrayList.add(input);
                map.put(input.getValue(), inputArrayList);
            } else {
                map.get(input.getValue()).add(input);
            }
        });
        return map;
    }


}
